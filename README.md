# Speech Recognition Library
To enable speech using Prayagad Speech Library include the files under src/lib directory. Following are the files to be included in your application
* SpeechRecognizer.js
* speechRecognitionLib folder in the same path and level where the SpeechRecognizer.js file is

Once these 2 steps are done your application can initialise and use speech recognition service. This service is tested to work on latest versions of Chrome and Firefox. 
Please see the sample code on how to use this library and functions/callbacks to be done

## Sample Speech Recognition Webapp

The scope of this application is to demonstrate the SR library's ability to
1. Detect speech and convert it to text (input being microphone).
2. Detect Keywords and use callbacks to take action on top of it.


## Running this application

```
npm install
npm start
```


## Usage and Callbacks available to use.

Intializing. This is to be done once per session. The same object has to be shared across multiple flows. For example across multiple EHR documents reuse the same object.

```
import SpeechRecognizer from './lib/SpeechRecognizer';
const speechRecognizer = new SpeechRecognizer();
```
Adding phrases to detect
```
speechRecognizer.addSpeechPhrases('sectionName','phrase');
speechRecognizer.addSpeechPhrases('medication','medicinal advice');
```

Adding callbacks
```
speechRecognizer.addStartListener(() => {
	console.log('Speech Recognition Started');
});

speechRecognizer.addStopListener(() => {
	console.log('Speech Recognition Stopped');
});

#### finalSectionTranscript comes when user pauses for a while or user stops the mic 
speechRecognizer.addFinalTextListener((finalSectionTranscript) => {
	console.log('Final Sectionwise Transcript' , finalSectionTranscript);
	// FinalSectionTranscript will have data in format
	// {
	//	"chiefComplaint" : "The user spoke in this section",
	// 	"medication"  :	"take crocin 3 times a day",
	// }
});

#### Interim text is the running text that comes as and when the speechRecognizer recognizes speech.
speechRecognizer.addInterimTextListener((text) => {
	console.log('Interim Text:'+text);
});

#### Whenever a keyword is detected , this callback is called
speechRecognizer.addDetectSectionListener((sectionName) => {
	console.log('Key Phrase Detected, corresponding Section is:"+sectionName);
});
```

Starting and Stopping SpeechRecognizer. It is recommended to provide control to user on when to start speech recognition and when to stop it along with indicator for the same.
```
### documentId can be any identifier that can be tagged for a particular patient visit record aka encounter
speechRecognizer.startVoiceCapture( null ,documentId);
speechRecognizer.stopVoiceCaputure();

```

