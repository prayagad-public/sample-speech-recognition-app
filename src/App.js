import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import icoMicOn from './images/ico_micon.png';
import icoMicOff from './images/ico_micoff.png';
import SpeechRecognizer from './lib/SpeechRecognizer';

class App extends Component {
  speechRecognizer = null;

  componentWillMount() {
    this.speechRecognizer = new SpeechRecognizer();
    this.speechRecognizer.addSectionPhrase('medication', 'medication');
    this.speechRecognizer.addSectionPhrase('investigation', 'investigation')
    this.speechRecognizer.addSectionPhrase('chiefComplaint', 'chief complaint')
    this.speechRecognizer.addSectionPhrase('chiefComplaint', 'subject')

    this.speechRecognizer.addStartListener(() => {
      this.setState({
        ...this.state,
        icoMic: icoMicOn,
        onClick: this.speechRecognizer.stopVoiceCapture,
      });
    });

    this.speechRecognizer.addStopListener(() => {
      this.setState({
        ...this.state,
        icoMic: icoMicOff,
        onClick: this.speechRecognizer.startVoiceCapture,
      });
    });

    this.speechRecognizer.addFinalTextListener((finalSectionTranscript) => {
      console.log('Final Section Transcript', finalSectionTranscript);
      const currentState = { ...this.state };
      currentState.chiefComplaint.interimText = '';
      currentState.investigation.interimText = '';
      currentState.medication.interimText = '';
      if (finalSectionTranscript.chiefComplaint != null) {
        currentState.chiefComplaint.text += finalSectionTranscript.chiefComplaint;
      }
      if (finalSectionTranscript.investigation != null) {
        currentState.investigation.text += finalSectionTranscript.investigation;
      }
      if (finalSectionTranscript.medication != null) {
        currentState.medication.text += finalSectionTranscript.medication;
      }
      // this.speechRecognizer.startSectionName = this.state.currentSection;
      this.setState({ ...currentState })
    });

    this.speechRecognizer.addInterimTextListener((text) => {
      const currentState = { ...this.state };
      currentState[currentState.currentSection].interimText = text;
      this.setState({ ...currentState })
    });


    this.speechRecognizer.addDetectSectionListener((sectionName) => {
      // Section Detected
      console.log('Section Detected', sectionName);
      this.setState({
        ...this.state,
        currentSection: sectionName
      });
      if (sectionName === 'chiefComplaint') {
        this.chiefComplaint.focus();
      }
      if (sectionName === 'medication') {
        this.medication.focus();
      }
      if (sectionName === 'investigation') {
        this.investigation.focus();
      }
    });

    this.speechRecognizer.addErrorListener((error) => {
      console.log('Error occurred', error);
    });

    this.setState({
      ...this.state,
      icoMic: icoMicOff,
      onClick: this.speechRecognizer.startVoiceCapture,
    });
  }

  state = {
    currentSection: 'chiefComplaint',
    chiefComplaint: { text: '', interimText: '' },
    investigation: { text: '', interimText: '' },
    medication: { text: '', interimText: '' },
    icoMic: null,
    onClick: null,
  };

  componentWillUnmount() {
    if (this.speechRecognizer != null) {
      this.speechRecognizer.destroy();
    }
  }

  render() {
    const styles = {
      color: '#000',
      marginBottom: '10px',
      width: '50%',
      lineHeight: '50px',
    };


    let chiefComplaintText = '';
    let investigationText = '';
    let medicationText = '';
    if (this.state.chiefComplaint.interimText != null && this.state.chiefComplaint.interimText.trim().length > 0) {
      chiefComplaintText = this.state.chiefComplaint.text + this.state.chiefComplaint.interimText;
    } else {
      chiefComplaintText = this.state.chiefComplaint.text;
    }
    if (this.state.investigation.interimText != null && this.state.investigation.interimText.trim().length > 0) {
      investigationText = this.state.investigation.text + this.state.investigation.interimText;
    } else {
      investigationText = this.state.investigation.text;
    }
    if (this.state.medication.interimText != null && this.state.medication.interimText.trim().length > 0) {
      medicationText = this.state.medication.text + this.state.medication.interimText;
    } else {
      medicationText = this.state.medication.text;
    }
    return (
      <div className="App">
        <div className="App-header">
          <img src={this.state.icoMic} alt="mic" style={{
            width: '60px',
            position: 'relative',
            top: '30px',
            zIndex: '999',
            // float: 'right',
            borderRadius: '50%',
            boxShadow: '0 10px 10px 8px rgba(100,100,100,0.3)'
          }} onClick={this.onMicClick.bind(this)} ref={(input) => { this.mic = input; }} />
          <h2>Sample Speech Recogition Client</h2>
        </div>
        <p className="App-intro">
          Use the phrases :  medication , investigation , chief complaint to change between sections
        </p>
        <input
          type='input'
          style={styles}
          underlineShow={true}
          value={chiefComplaintText}
          onChange={this.onTextChange.bind(this)}
          onFocus={() => {
            this.setState({ ...this.state, currentSection: 'chiefComplaint' })
          }}
          ref={(input) => { this.chiefComplaint = input; }}
        />
        <br />
        <input
          type='input'
          style={styles}
          underlineShow={true}
          value={investigationText}
          onChange={this.onTextChange.bind(this)}
          ref={(input) => { this.investigation = input; }}
          onFocus={() => {
            this.setState({ ...this.state, currentSection: 'investigation' })
          }}
        />
        <br />
        <input
          type='input'
          style={styles}
          underlineShow={true}
          value={medicationText}
          onChange={this.onTextChange.bind(this)}
          ref={(input) => { this.medication = input; }}
          onFocus={() => {
            this.setState({ ...this.state, currentSection: 'medication' })
          }}
        />
        <br />

      </div>
    );
  }

  onTextChange = (event) => {
    console.log("text", event.target.value);
    const currentState = { ...this.state };
    currentState[currentState.currentSection].text = event.target.value;
    this.setState({ ...currentState })
  }

  onMicClick = () => {
    this.speechRecognizer.startSectionName = this.state.currentSection;
    this.state.onClick();
  }

}
export default App;